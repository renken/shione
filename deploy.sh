#!/bin/sh

set -eu

hugo --minify

rsync \
  -a \
  --delete \
  --progress \
  public/ \
  'root@shione:/var/www/html/shione.net'
