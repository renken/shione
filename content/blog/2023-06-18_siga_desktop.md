+++
title = "Siga desktop has joined the game."
date = "2023-06-18T18:14:00"
slug = "siga-desktop"
tags = ["hw"]
+++

Around the 25th of May, I ended up building siga in its Desktop PC form. It's
entirely based on AMD because I wanted the best possible compatibility with
GNU/Linux. It's running KDE on Debian stable and I'm enjoying it so far!

## Specifications

| Component        | Model                                      |
|------------------|--------------------------------------------|
| CPU              | AMD Ryzen™ 5 7600X                         |
| CPU cooler       | be quiet! Dark Rock Pro 4                  |
| Motherboard      | ASUS TUF GAMING A620M-PLUS                 |
| Memory           | VENGEANCE® 32GB (2x16GB) DDR5 DRAM 5600MHz |
| Storage          | Crucial P5 Plus 1TB                        |
| Video card       | AMD Radeon™ RX 6700                        |
| Case             | Corsair 4000D Airflow                      |
| Power supply     | Corsair RM750x 80 PLUS Gold                |
| Operating system | GNU/Linux Debian bookworm                  |
| Monitor          | LG UltraGear 27GP850P-B                    |
| Peripherals      | Cudy AX5400 Tri-Band Wi-Fi 6 PCIe Adapter  |

## Backups

I am now using [borgbase](https://borgbase.com) to backup my personal data
using their graphical front-end of [borgbackup](https://www.borgbackup.org)
[Vorta](https://vorta.borgbase.com) which I'm happy with so far! I highly
recommend checking borgbase especially if you wish to have a good deduplication
ratio, append-only backup system.
