+++
title = "Frustration with web developers"
date = "2022-04-02"
slug = "frustration-with-web-devs"
tags = ["web", "sysadm"]
+++

Past few days, at least 5 websites rejected my email address
`mail@renken.systems` because it doesn't match their regular expression. I
might consider switching to something shorter/common that isn't taken already.

I was thinking of renken.no maybe but that would imply I am from/live in Norway
which isn't an assumption I want others to have. I'll see what I can do about
this.
