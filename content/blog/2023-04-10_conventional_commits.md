+++
title = "Conventional commits"
date = "2023-04-10T12:47:00+01:00"
slug = "conventional-commits"
tags = ["prog", "git", "cicd"]
+++

I recently came across [conventional
commits](https://www.conventionalcommits.org/en/v1.0.0/) and I find the idea
very interesting and practical. The commit message style is similar to my
subjective style except it's standardized and most importantly parsable. The end
goal is to automatically generate changelogs following [keep a
changelog](https://keepachangelog.com/en/1.0.0/).

I found a couple of tools which does this.

## commitizen

[commitizen](https://github.com/commitizen-tools/commitizen) is good if you use
the default config. However, as soon as you derive from the latter, you'd need
to write your own customized parsing config which I found to be somewhat
difficult to write considering how sometimes commitizen crashes if your config
is invalid.

I like how nicely it generates changelogs for you, I should probably take more
time to write a working custom config that suits my needs.

## git-cliff

[git-cliff](https://github.com/orhun/git-cliff) looks very interesting as well.
It is similar to commitizen. However the command arguments feel more natural to
me I feel. I quickly tried it out, I remember `link_parsers` not working but it
might've been a misconfiguration on my end. We'll see.
