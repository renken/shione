+++
title = "My manga collection is growing!"
date = "2022-06-01"
slug = "my-manga-collection-growing"
tags = ["manga", "jp", "pers"]
+++

I visited [fnac](https://www.fnac.com) around a month ago or so and noticed
that they sell manga. I wasn't aware that the French are huge weebs, they have
a wide variety of titles, separated by genres mainly shounen, shoujo and seinen
and *all* of them are in French.

While the majority of their selection consists of popular and recent titles out
there, you might also find niche ones like Yokohama Kaidashi Kikou and Yotsuba.
My first ever purchased physical copy was Yotsuba volume 1 which I enjoyed
reading very much, I'm looking forward to buying the second and third volumes
soon.

Yesterday I ended up buying YKK's first three volumes because I figured gazing
at the landscapes on paper would make me appreciate it even more and I was
right! They're also helping me improve my French too which is nice.

![My manga collection 11/06/2022](./2022-06-11_manga_collection.jpg)
