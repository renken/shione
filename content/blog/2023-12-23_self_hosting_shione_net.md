+++
title = "Self-hosting shione.net"
date = "2023-12-23T18:47:00"
slug = "self-hosting-shione"
tags = ["sysadm", "shione"]
+++

I recently managed to get my hands on a mini PC, [Union NiPoGi CK10 -
10810U](https://www.notebookcheck.net/NiPoGi-CK10-with-the-Intel-Core-i7-10810U-reviewed-Small-office-PC-offering-space-for-three-SSDs.671211.0.html)
thanks to my friend Lucien and thought it'd be a good opportunity to host my
own server to experiment with a bunch of stuff.

## Services

### Synchronizing my personal devices

I'd like to share/synchronize files among my PC, laptop, server and phone. The
first layer of authentication would be having access to WireGuard VPN. As for
how to share the files themselves, I am not entirely sure yet, it could be as
simple as using SFTP. However, I am also interested in investigating
[Nextcloud](https://nextcloud.com/), specifically [Nextcloud
Files](https://nextcloud.com/files/) because of it's well supported on all
platforms, I think KDE's Dolphin has a smooth Nextcloud integration as well by
default but haven't tried it yet.

### Hosting a Git service

It could be something as simple as cgit or as interesting as
[sourcehut](https://git.sr.ht/)/[gitea](https://about.gitea.com/). I will have
to figure out how to setup CI/CD, at least a very basic version of it however.

### SRB2Kart server

It would be fun to have it running in the background, or at least launched on
request. However, I'll need to figure out a way to secure it since the
server-side code seems not that high quality to trust completely (perhaps I
could help with that). I will need to do the same for others as well, we'll
see. I wonder if SELinux will be enough to separate the servers from each
other.

### CS2 demo analyzer

Potentially a fun hobby project I could work on and expose to the public. It'll
just print stats about the uploaded CS2 demo file and other misc stuff.

### Multiplayer Nonogram game

I like nonograms, even though I don't get to play them often, and I'd like to
implement it myself potentially to learn WASM and Rust's ecosystem of it.

## Deployment

I like the idea behind GNU Guix and Nix. However, for my own lazy and practical
reasons, I'd rather stick to GNU/Linux Debian to avoid having to deal with all
the per package-specific issues that comes with packaging for a younger/less
popular package manager.

Ideally configuring the server would be traced, less repetitive and idempotent.
I took a glance at [ansible](https://www.ansible.com/). However, I do not like
the fact that YAML is used here with no possible alternative bothers me.

I came across another solution called
[pyinfra](https://github.com/pyinfra-dev/pyinfra) which is exactly what I want
except that I do not get why it has such a complicated setup as well as the
fact that it's not packaged in Debian, nor most GNU/Linux distros I suppose? I
personally find the idea of using Python for such tasks, as well as other
automation tasks, very practical because it's available on most distros by
default nowadays and the packaging system is becoming less of a pain to handle.
The language is expressive, mature and performance does not really matter in
this case.

I decided to experiment with
[`config-package-dev`](https://debathena.mit.edu/config-package-dev/) instead
for the mean time. I must admit that the Debian packaging system feels very
unintuitive to me and I certainly don't like having to deal with Perl but I
should get used to it in order to be able to contribute back to GNU/Linux
Debian.

---

*Update: 2023-12-24 21:44*

This page was first served on self-hosted shione!
