+++
title = "Graduation and moving forward with Munic"
date = "2022-07-12"
slug = "graduation-munic"
tags = ["pers", "fr", "work"]
+++

I graduated from my school last Thursday, I am officially a certified (tm)
engineer! Consequently, I'm joining Munic as a full-time employee after my
internship is done.

I have been dabbling a bit with Zephyr and drivers recently, it's quite nice. I
need to figure out how to make clang-tidy work under a Zephyr-based project.
