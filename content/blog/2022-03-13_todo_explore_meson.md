+++
title = "TODO: Explore meson"
date = "2022-03-13"
slug = "todo-explore-meson"
tags = ["prog", "todo", "c/c++"]
+++

I should definitely play around with meson one day, maybe add template/c support
for it. I think working on template/c is a good entry to understanding how build
systems work and how to configure a real-world project using meson.

I will start publishing such TODOs publicly, maybe that helps with actually
working on them.

---

Update: 2024-08-17 01:01

I forgot to mention that I ended up fiddling around with it a bit for
[kvps](https://gitlab.com/renken/kvps) a while ago.
