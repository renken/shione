+++
title = "Random rant"
date = "2022-03-12"
slug = "random-rant"
tags = ["pers"]
+++


## The lurking problem

It's been quite a while since I wrote my last blog post. While I've been always
thinking that the reason behind this was the lack of having interesting/advanced
subjects to talk about, I came to the realization that it was in fact related to
consuming media and constantly "lurking" instead of engaging in things.

It is definitely easier to read a well-written blog post shared on news
aggregation websites or skim through an interesting thread as opposed to writing
your own or contributing to that thread. I think about how I have nothing to
add, how I lack expertise in such advanced topics... etc, but in reality, I'm
just holding myself from doing so because it's easier not to engage, it's easier
to be lazy.

## Imperfection

It's easier to find excuses as for why my next project will suck, not be used by
others and so on compared to simply working on it. I think my main problem is
losing motivation over time, I need to figure out how to get motivation out of
the equation and possibly discipline myself more.

## Close-up

Oh well, my thoughts are still unclear and messy but I will push them online
anyway. It's something I plan on working on over time not ship only after it's
done!
