+++
title = "Switching to Markdown"
date = "2022-07-12T23:59:00+02:00"
slug = "switching-to-markdown"
tags = ["sysadm", "shione"]
+++

Sphinx supports Markdown through `myst-parser` as documented
[here](https://www.sphinx-doc.org/en/master/usage/markdown.html). This is nice
because most of the time I am writing Markdown especially on GitLab, I don't
have a hard dependency on RestructuredText itself meaning I can safely just
make the switch.
