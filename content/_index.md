+++
title = "renken's weblog"
+++

<p align="center">
<img
    style="text-align: center;"
    src="/avatar.png"
    name="renken's avatar"
    alt="bunny's typical sunday"
    width="148" height="148">
</p>

Hi, I’m Renken, 25 years old from Algeria and I manage shione. The name
Shione translates to “sound of tide” (汐音).

<dl>
  <dt>email</dt>
  <dd><a href="mailto:renken@shione.net">renken@shione.net</a></dd>
  <dt>git</dt>
  <dd><a href="https://git.shione.net">shione/renken</a> - <a href="https://gitlab.com/renken">gitlab/renken</a></dd>
  <dt>telegram</dt>
  <dd><a href="https://t.me/renken">renken</a></dd>
  <dt>last.fm</dt>
  <dd><a href="https://www.last.fm/user/renkenrc">renkenrc</a></dd>
  <dt>pgp</dt>
  <dd><a href="/renken.asc">53334B09D2066FCF</a></dd>
  <dt><a href="/services/stream.html">stream</a></dt>
  <dd><a href="srt://shione:60000">srt://shione:60000</a></dd>
</dl>
