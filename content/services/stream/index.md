---
title: Live stream
---

# SRT stream

I occasionally stream videos games or miscellaneous stuff to my friends over
[SRT](https://en.wikipedia.org/wiki/Secure_Reliable_Transport) at
[srt://shione.net:60000](srt://shione.net:60000). 

# Connecting to a passphrase-protected stream

For those who received a passphrase to connect to a supposedly private stream,
the URL becomes `srt://shione.net:60000?passphrase=PASSPHRASE_HERE`.

## Media players

### mpv

Your mileage may vary, feel free to tweak the options a bit for your own
comfort.

```console
$ mpv --no-resume-playback --no-pause --cache-pause-wait=0.1 'srt://shione.net:60000'
```

### VLC

OBS has a wiki page on how to setup VLC to watch an SRT stream, see [VLC
usage](https://obsproject.com/wiki/Streaming-With-SRT-Or-RIST-Protocols#vlc-usage).

## Deployment

### `srt-live-transmit`

Under the hood, I use
[`srt-live-transmit`](https://github.com/Haivision/srt/blob/master/docs/apps/srt-live-transmit.md)
from [`srt-tools`](https://packages.debian.org/stable/utils/srt-tools) because
my use case is extremely simple. However, I should invest more time into
understanding how I can deliver a better quality stream with less/no artifacts.
