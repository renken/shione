+++
title = "Services"
datetime = false
show_tags = false
+++

Shione offers some services either exclusively for private personal use such as
[nextcloud](https://nextcloud.com) or for public/semi-public purposes such
[live streaming](/services/stream.html).

Have a look at [nichijou](https://gitlab.com/renken/nichijou) for the
administration behind these services.

> TODO: Generate a table of content for all the services documented here.
{.todo}
