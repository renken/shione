+++
title = "Miscellaneous links"
toc = false
show_tags = false
+++

# Multimedia

- [Birthday drawings](/misc/p/birthday.html)

# Archived

- [German notes](/misc/archived/de.html)
- [MQTT notes](/misc/archived/mqtt.html)
