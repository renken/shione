---
title: Birthday drawings
layout: page
---

# Happy birthday!!

I'd like to thank pika and verm for this lovely picture, it never fails to put
a smile on my face. Hacker/Artist pika can be found on
[Pixiv](https://www.pixiv.net/en/users/46770896).


<figure style="text-align: center;">
    <img 
        src="./birthday/renkenwaifu.png" 
        name="nonoka-birthday"
        alt="nonoka-birthday" 
        width="407.5px" 
        height="671.75px">
    <figcaption>
        <a href="./birthday/renkenwaifu.kra">renkenwaifu.kra</a>
    </figcaption>
</figure>

# Wednyasday's nyan night!!

pikanyan drew another picture of nonoka on a late Wednesday night comfy stream
and it's amazing. I love it!

<figure style="text-align: center;">
    <img 
        src="./birthday/nonokasayshi.png" 
        name="nonoka-wednyasday"
        alt="nonoka-wednyasday"
        width="50%" 
        height="50%">
    <figcaption>
        <a href="./birthday/nonokasayshi.kra">nonokasayshi.kra</a>
    </figcaption>
</figure>

# Original Do NOT Steal!!

pikapyon drew yet another amazing picture, this time with the three of us
together. From left to right.

1. ximin @ [ximinity.net](https://ximinity.net)
2. verm @ [mel.vin](https://mel.vin), spam him with emails if his website still
   doesn't have a proper homepage
3. renken @ [shione.net](https://shione.net)

<figure style="text-align: center;">
    <img 
        src="./birthday/originalOCdonotsteal.png" 
        name="nonoka-wednyasday"
        alt="nonoka-wednyasday"
        width="80%" 
        height="80%">
    <figcaption>
        <a href="./birthday/originalOCdonotsteal.kra">originalOCdonotsteal.kra</a>
    </figcaption>
</figure>

# Happy nichijou birthday!!

pikadesu picked an amazing theme for this year's birthday's gift, nichijou! I
really love it and I'm looking forward to using sakamoto as my avatar in case
nonoka was considered too much for wörk :^) Thank you very much for this
pikadesu!

<figure style="text-align: center;">
    <img 
        src="./birthday/renkenbirthdaynichijou.png" 
        name="nonoka-wednyasday"
        alt="nonoka-wednyasday"
        width="80%" 
        height="80%">
    <figcaption>
        <a href="./birthday/renkenbirthdaynichijou.kra">renkenbirthdaynichijou.kra</a>
    </figcaption>
</figure>
