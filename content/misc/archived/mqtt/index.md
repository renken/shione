---
title: MQTT notes
layout: page
---

I first came across the MQTT protocol when I was looking for possible
alternative ideas and implementations to Syncplay. The key concept was that
video players such as mpv would connect to a central server, share information
about their media they're playing and synchronize their state with other peers.

MQTT stands for Message Queuing Telemetry Transport and is an open OASIS and ISO
standard. It is designed to be lightweight and primarily for as a
publish-subscribe protocol which is what synchronization-based software such as
Syncplay *should* use. Another aspect of MQTT that makes it a viable design
option for media synchronization is its simplicity of design. That leaves the
implementer with a small well-defined logic to implement which means higher
chances of an efficient implementation and less unintentional errors. It also is
reasonable to go for a minimal transport protocol to provide a minimal service
such as media synchronization.

I'll be reading through the standard version 5.0.0 published in 07 March 2019
and trying to provide a UML-based representation of the MQTT standard with few
notes to the C implementation. Keep in mind that at the time of writing this, I
do not plan on implementing myself but I believe these notes would be helpful
for anyone wishing to implement MQTT themselves.

Another note to keep in mind is that I do not have prior MQTT experience so I'm
not familiar with "best solutions" or "practice workarounds". I'll solely view
MQTT through the lens of the standards I have downloaded. Possible notes *may*
be added in the future.

* Keep in mind that I'm by no means an expert and have very little experience
  with real-word software. I'll be researching things I do not understand and
  share my explanation here if possible. I suggest you double-check every
  information provided here as well in order to spot any misconception or
  misunderstanding.

* Do note expect a strict and regulated publishing schedule, I'll only read the
  standard and write about it in my free time whenever I wish. You *may*
  contribute if you wish too. Alternatively you may advance on your own.

* I *may not* respect the styling and indentation of the standard e.g., I *may*
  choose to merge a sub-header back with its parent-head.

Below are notes about each chapter in separate pages in the order provided by
the standard.

# Introduction

## Terminology

MQTT is a server-client protocol in which all clients *must* connect to a server
in order to exchange information which have to go through the server.

## Network connection

The standard defines network connection as the following.

> A construct provided by the underlying transport protocol that is being
	used by MQTT. <br>
> * It connects the Client to the Server.
> * It provides the means to send an ordered, lossless, stream of bytes in
	  both directions.

I believe that this definition was mainly provided as to not limit the network
connection, specifically the network protocol, to TCP as it is possibly the most
common network protocol used when implementing MQTT. You can emulate the bullet
points under non-TCP network protocols e.g., UDP and implement MQTT on top of
it.

## Session

> A stateful interaction between a Client and a Server. Some Sessions last
	only as long as the Network Connection, others can span multiple
	consecutive Network Connections between a Client and a Server.

This allows for the possibility of attempting re-connection after it has been
lost to preserve the same session.

## Application message

The information sent between clients through servers is called the application
message which is carried by the MQTT protocol across the network. It contains
payload data, a Quality of Service (QoS), a collection of properties and a topic
name.

### Subscription

A subscription comprises a topic filter and a maximum QoS. A subscription is
associated with a single session. A session can contain more than one
subscription. Each subscription within a session has a different topic filter.

Shared subscription can be associated with more than one session. I assume this
is used for the case of multiple servers? I don't see the need to have multiple
sessions to the same server but it is allowed by the standard.

Wildcard subscription is a subscription with a topic filter containing one or
more wildcard characters. This allows the subscription to match more than one
topic name. Basically ``*`` in regular expression.

> TODO: write UML diagram about  user <-- session 0..* -- 1..1 subscription
{.todo}

### Topic name

The label attached to an application message which is matched against the
subscriptions known to the server. This is up to the application to use it
efficiently. In the case of media synchronization, would you rather use do
central topics e.g., ``/media/filename`` and users push to that topic or would
you do per-user topics ``/renken/media/filename`` and only ``renken`` can
publish to that topic. Similar ideas to that come to mind I guess.

### Topic filter

An expression contained in a subscription to indicate an interest in one or more
topics. A topic filter can include wildcard characters.

### Client

As discussed before, in MQTT, the client can only communicate with a server and
not with other clients. Note that the client is not limited to a single server.
It also not limited to a fixed number of application messages published or
requested.

The server acts as an intermediary between clients which publish application
messages and clients which have made subscriptions.

<figure style="text-align: center;">
    <img 
        src="https://www.planttext.com/api/plantuml/svg/ZSyn3i8m38NXdLFu0bo0WILYHweuG18zQ4Hg5pifrsypm50oiFlxzUno0XSwdcc0Bj0JNQpua9EDCFB4A6_H9qLXHYn9-E1-oBL-0PEVvvoYRwSqmVm37J7KO55Js4WHKhmghEnfizR1DxOTdsJ1DlRJkOl-st9Ph5L6z5vT0G00" 
        name="client UML diagram"
        alt="client UML diagram">
    <figcaption>
        <a href="https://www.planttext.com/?text=ZSyn3i8m38NXdLFu0bo0WILYHweuG18zQ4Hg5pifrsypm50oiFlxzUno0XSwdcc0Bj0JNQpua9EDCFB4A6_H9qLXHYn9-E1-oBL-0PEVvvoYRwSqmVm37J7KO55Js4WHKhmghEnfizR1DxOTdsJ1DlRJkOl-st9Ph5L6z5vT0G00">client UML diagram</a>
    </figcaption>
</figure>
