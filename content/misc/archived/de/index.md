---
title: German notes
date: 2019-04-23
layout: page
---

An abandoned series of posts highlighting things I have learned about the
German language.

# Chapter 1: introduction

## Cognates

A cognate is a word that is derived from the same original form such as kühl and
cool. We'll explore some consonant relationships that exist between German and
English to make guessing more accurate and faster but of course, always consider
checking the dictionary for the definitive answer.

| German | English | Examples |
| --- | --- | --- |
| f, ff (medial or final) |  p | hoffen - to hope, scharf - sharp |
| pf |  p, pp | Apfel - apple, Pfeife - pipe |
| b (medial or final) | v or f | geben - to give, halb - half |
| d | th | Ding - thing |
| ch | k | Buch - book, machen - to make, suchen - to seek |
| cht | ght | Macht - might, Sicht - sight, Recht - right, Nacht - night |
| g | y or i | sagen - to say, legen - to lay, Nagel - nail, fliegen - to fly |
| k | c | kommen - to come, kritisch - critical |
| s, ss, ß (medial or final) | t | hassen - to hate, grüßen - to greet, besser - better, Fuß - foot |
| tz, z | t | Katze - cat |
| t | d | trinken - to drink, kalt - cold, Tochter - daughter |

Note that sometimes it may require some flexibility with vowels to get the
correct word e.g., hören - to hear. Try to figure out the following words: Haus,
lassen, Sonne, Wort.

Good? What about Pfefferminze and tanzen (try your luck with stripping -n/-en if
you think it's a verb in its infinitive form)

Okay let's learn proverbs. Well, first there's this thing with German where it
is possible to "glue" multiple words to produce another word referred to as
compounds for example Sprichwörter means proverbs! look both of them up. Now for
the actual proverbs.

* Blut ist dicker als Wasser. Blood is dicker(thicker) als(as)
  Wasser(ss->t).
* Reiche Leute(people) haben fette Katzen.


Book titles! Buchtitel, apparently used for both singular and plural.

* Yasmina Khadra, was der Tag der Nacht schuldet (haven't read it yet by the
  way).

## Genders

.. note::
	Find a way to ease the process of memorizing these? being able to
	quickly remember and map the endings would be very helpful.

Complex stuff, unsure how to deal with it other than just practicing it often.
But with what?

It's very important to know what your subject's gender is to understand the
context of the passage in hand. The corresponding German definite articles are
as follows

| Gender | Article |
| --- | --- |
| Masculine | der |
| Feminine | die |
| Neuter | das |
| Plural | die |

### Masculine

1. Nouns denoting male beings.
2. Most nouns ending with -er that are agents of a specific activity e.g., der
   Computer.
3. The days, months and seasons. Look them up!
4. Nouns ending in -ig, -ich, -ing, -ast, -mus e.g., König (king), der
   Socialismus (Socialism).
5. Points of compass. Norden, Süden, Oster und Westen.

### Feminine

1. Nouns denoting female beings.
2. Nouns of most trees, fruits and flowers. Look your favorites up!
3. Nouns ending with -er, -ie, -ik, -in, -ion, -hiet, kiet, -schaft, -tät,
   -ung, -ur e.g., die Gesundheit (health), die Gesellschaft (society) und die
   Hoffung.

### Neuter

> Diminutive nouns usually have an umlaut if the stem vowel is a, o, u or au
so a becomes ä...
{.info}

1. Nouns with diminutive endings -chen, -lein e.g., das Buch -> das Büchlein,
   das Männlein.
2. Most nouns of foreign origin that end in -o e.g., das Auto.
3. Infinitives used as a noun e.g., das Kommen.
4. Nouns ending with -ium, -um  e.g., das Vism.

## Compounds

New words in German sometimes can be formed by combining simpler words. Some of
these words can be particularly graphic. For example.

* Abend (evening) + Land (country) = (occident).
* Morgen + Land = (orient).
* Morgen + Röte = (dawn).
* Eier (eggs) + Auflauf(crowd) = Eierauflauf (soufflé).

The gender of the noun is determined by *its final component*. Thus, even in a
word as long as Unfallversicherungsgesellschaft (accident-insurance-society),
the article will be *die* because the suffix *-schaft* is feminine. This seems
difficult...

* Standard German is known as Hochdeutsch, try figuring that out. Northen dialects
  are referred to as Platt which you *should* be able to guess.
* Sie haben eine nette Dreizimmerwohung etwa 10 kilometer von der Stadtmitte.
* Deutschland ist eine Bundesrepublic in diverse Länder oder Bundesländer
  unterteilt. Die Länder habt lokal Kontrolle uber Bildung und jeder hat sein
  Landesregierung.

# Chapter 2: Pronunciation

This, in addition to the tables provided by Colloquial German, should serve as a
good document for how German pronunciation is like. Of course, you *should*
always look up the pronunciation in its audio format and/or learn how to read
IPA table efficiently to improve your pronunciation skills.

This is also helpful to have a, sometimes misleading, sometimes correct guess of
the word's definition simply by saying them. For example, jung is pronounced
young and indeed does mean young.

Keep in mind that all German nouns are capitalized e.g., Buch for book.

## Vowels

> Keep in mind that I myself don't know most of these words. They're used
	to simply give an example on how German sounds like in a way. It'd be
	good if you pick up a word or two from this guide. I don't think you'll
	be revisiting this page much. Maybe I'll consider updating the tables
	with better/more fitting examples that you *may* find interesting.
{.info}

Vowels are either short or long like most languages. They are long when

* They are doubled: Paar (Pair), Haar (hair), Schnee (Snow).
* They are followed by h: sehen (to look. Sehen is the noun view, notice the
  capitalization matters!), Jahr (year), Ohr (ear).
* They are followed by a single consonant: gut (good), rot (red).

They are shower when

* They are followed a double consonant: Bett (bed), Mann (man), hoffen (to
  hope).
* They are followed by two or more consonants: sitzen (to sit), ernst (serious,
  look it up!).

| Vowel | Type | English equivalent sound | German words |
| --- | --- | --- | --- |
| a | long | father | Vater, haben, sagen |
| a | short | hot | Vasser, Hand, alt |
| e | long | may | See, geben |
| e | short | let | Ende |
| i | long | greet | Tiger, Universität |
| i | short | sit | ist, dick, Mitte, Mittag, Mittwoch |
| ie | long | similar to here, look up how the following are pronounced. | Bier, hier, fliegen, liegen |
| o | long | open | Sohn, Brot, Segelboot |
| o | song | Sonne, Sommer | (none) |
| u | long | dune | Blume, Pudel, Handschuh |
| u | short | bush | Mutter, und, unter |
